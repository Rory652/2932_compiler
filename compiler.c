/************************************************************************
University of Leeds
School of Computing
COMP2932- Compiler Design and Construction
The Compiler Module

I confirm that the following code has been developed and written by me and it is entirely the result of my own work.
I also confirm that I have not copied any parts of this program from another person or any other source or facilitated someone to copy this program from me.
I confirm that I will not publish the program online or share it with anyone without permission of the module leader.

Student Name: Rory Coe
Student ID: 201407009
Email: el20rdc@leeds.ac.uk
Date Work Commenced: 02/03/2022
*************************************************************************/

#include "compiler.h"

#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int getFiles(char* directory, char** toStore);

int InitCompiler() {
	return InitSymbols();
}

ParserInfo compile(char* dir_name) {
	ParserInfo p;
    p.er = none;

    char **files = malloc(sizeof(char *) * 128);
    int numFiles = getFiles(dir_name, files);

    // Parse each file and construct symbol table
    for (int i = 0 ; i < numFiles; i++) {
        // Make sure file actually exists - in case I mess up paths somewhere
        if (InitParser(files[i]) == 1) {
            p = Parse();
            StopParser();
        } else {
            printf("Error: %s wasn't found\n", files[i]);
            exit(-1);
        }

        if (p.er != none) {
            break;
        }
    }

    if (p.er == none) {
        ParserInfo check = checkMentioned();
        if (check.er != none) {
            return check;
        } else {
            check = checkFinalTypes();
            if (check.er != none) {
                return check;
            }
        }
    }

	return p;
}

int StopCompiler() {
	return StopSymbols();
}

int getFiles(char* directory, char** toStore) {
    DIR *d;
    struct dirent *dir;
    int pos = 0;
    int length;

    d = opendir(directory);

    if (d) {
        // Read everything in the file
        while ((dir = readdir(d)) != NULL) {
            // Check that it's a .jack file
            if ((length = strlen(dir->d_name)) > 3 && !strcmp(dir->d_name + length - 5, ".jack")) {
                toStore[pos] = malloc(sizeof(char) * 512);
                // Add directory path to file name
                strcpy(toStore[pos], directory);
                strcat(toStore[pos], "/");

                // Copy file to toStore array
                strcat(toStore[pos], dir->d_name);
                pos++;
            }
        }
        closedir(d);
    }

    return pos;
}

#ifndef TEST_COMPILER
int main() {
	InitCompiler();
	ParserInfo p = compile("../Jack Files/UNDECLAR_CLASS");
    printf("%i\n", p.er);
    printf("%s\n", p.tk.lx);
	StopCompiler();
	return 1;
}
#endif
