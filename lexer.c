/************************************************************************
University of Leeds
School of Computing
COMP2932- Compiler Design and Construction
Lexer Module

I confirm that the following code has been developed and written by me and it is entirely the result of my own work.
I also confirm that I have not copied any parts of this program from another person or any other source or facilitated someone to copy this program from me.
I confirm that I will not publish the program online or share it with anyone without permission of the module leader.

Student Name:           Rory Coe
Student ID:             201407009
Email:                  el20rdc@leeds.ac.uk
Date Work Commenced:    01/02/2022
*************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "lexer.h"


// YOU CAN ADD YOUR OWN FUNCTIONS, DECLARATIONS AND VARIABLES HERE

// Function and Struct Definitions
// Struct containing raw data from source file
typedef struct {
    FILE *file;
    char *fileName;
    int lineNumber;
    int linePos;
    char line[200];
} Source;

// Global variables
Source source;
char types[7][10] = {"RESWORD", "ID", "INT", "SYMBOL", "STRING", "EOFile", "ERR"};

// Finds the next valid line and stores it in source.line with whitespace and comments removed
int getNextLine();
// Removes new line characters and leading whitespace from a string
char* formatLine(char *str);
// Removes trailing whitespace from a string
char* removeTrailingSpaces(char *str);

// Gets the next valid token (for both Peek and Get) and stores the end position of it in back
Token findNextToken(int *back);

// Checks if a string is a valid keyword
int isKeyword(char *lexeme);
// Checks if a character is a valid symbol
int validSymbol(char c);

// Function Implementations
int getNextLine() {
    int comment = 0; // Tracks whether current line is part of a multi-line comment (/* to */);
    int valid = 0;

    while (valid == 0) {
        if (fgets(source.line, 200, source.file) != NULL) {
            char *temp = formatLine(source.line);

            // Copies string - can't use strcpy() as it causes errors when strings overlap
            for (int i = 0; i <= strlen(temp)+1; i++) {
                source.line[i] = temp[i];
            }

            source.lineNumber++;
            source.linePos = 0;

            // If line is part of multi-line comment
            if (comment == 1) {
                // Line is part of a multi-line comment
                char *search;
                // Checks if it has a comment terminator
                if ((search = strstr(source.line, "*/")) != NULL) {
                    search += 2;

                    for (int i = 0; i <= strlen(search)+1; i++) {
                        source.line[i] = search[i];
                    }

                    comment = 0;
                }
            }

            // Not part of same if-else statement as a comment can end mid-line and I need to check beyond that
            if (comment == 0) {
                /* Lines can immediately be discarded if they:
                 * Are empty (size = 0)
                 * Start with '//' - as whole line will be a comment
                 */
                if (strlen(source.line) > 0 && strncmp(source.line, "//", 2) != 0) {
                    char *search;
                    // Check for any comments at the end of the line and removes them
                    if ((search = strstr(source.line, "//")) != NULL) {
                        search[0] = '\0';
                    }

                    // Check for any multi-line comments/enclosed comments
                    while((search = strstr(source.line, "/*")) != NULL) {
                        char *end;
                        if ((end = strstr(search, "*/")) != NULL) {
                            end += 2;

                            for (int i = 0; i <= strlen(temp)+1; i++) {
                                search[i] = end[i];
                            }
                        } else {
                            comment = 1;
                            search[0] = '\0';
                        }
                    }

                    // Discard empty strings
                    if (strlen(source.line) > 0) { valid = 1; }
                }
            }
        } else {
            if (comment == 1) {
                // Error: EOF in comment
                return -2;
            }

            return -1;
        }
    }

    // Get rid of any whitespace at the end of the formatted string
    char *temp = removeTrailingSpaces(source.line);

    for (int i = 0; i <= strlen(temp)+1; i++) {
        source.line[i] = temp[i];
    }

    return 1;
}

char *formatLine(char *str) {
    // Remove new line characters
    str[strcspn(source.line, "\r\n")] = 0;

    // Trim leading space and tabs
    while(isspace((unsigned char)*str) || (unsigned char)*str == 9) { str++; }

    return str;
}

char* removeTrailingSpaces(char *str) {
    char *end = str + strlen(str) - 1;

    // Decrement end as long as spaces are still being found
    while(end > str && (isspace((unsigned char)*str) || (unsigned char)*str == 9)) { end--; }

    // Change end position of string
    end[1] = '\0';

    return str;
}

Token findNextToken(int *back) {
    Token t;

    // Skip over any whitespace at the start of the string
    while (source.line[source.linePos] == ' ' || source.line[source.linePos] == 9) { source.linePos++; }

    int eof = 0;

    if (source.linePos >= strlen(source.line) || source.line[source.linePos] == '\0') { eof = getNextLine(); }

    t.ln = source.lineNumber;

    for (int i = 0; i <= strlen(source.fileName)+1; i++) {
        t.fl[i] = source.fileName[i];
    }

    if (eof == -1) {
        // End of file found
        t.ln++;
        t.tp = EOFile;
        strcpy(t.lx, "End of File");
        return t;
    } else if (eof == -2) {
        // Comment in end of file
        t.ln++;
        t.tp = ERR;
        t.ec = EofInCom;
        strcpy(t.lx, "Error: unexpected eof in comment");
        return t;
    }

    char first = source.line[source.linePos];
    int end = source.linePos+1;

    if (isalpha(first) || first == '_') {
        // Keyword or Identifier
        while (isdigit(source.line[end]) || isalpha(source.line[end]) || source.line[end] == '_') {
            // Keep iterating until end of the lexeme is found
            end++;
        }

        strncpy(t.lx, source.line + source.linePos, end - source.linePos);
        // Ensure string ends at the correct point - otherwise errors can happen in isKeyword()
        t.lx[end - source.linePos] = '\0';

        // Check if lexeme is a reserved keyword
        if (isKeyword(t.lx) == 1) {
            t.tp = RESWORD;
        } else {
            t.tp = ID;
        }
    } else if (isdigit(first)) {
        // Number
        while (isdigit(source.line[end])) {
            // Keep iterating until end of the lexeme is found
            end++;
        }

        strncpy(t.lx, source.line + source.linePos, end - source.linePos);

        t.tp = INT;
    } else if (first == '"') {
        // String literal
        while (source.line[end] != '"' && end <= strlen(source.line)) {
            // Keep iterating until end of the lexeme is found
            end++;
        }

        // Line ends before string literal does
        if (end > strlen(source.line)) {
            t.tp = ERR;
            int isEOF = getNextLine();

            // Check if end is EOF or a New Line
            if (isEOF == -1) {
                t.ec = EofInStr;
                strcpy(t.lx, "Error: unexpected eof in string constant");
            } else {
                t.ec = NewLnInStr;
                strcpy(t.lx, "Error: new line in string constant");
            }

            return t;
        }

        strncpy(t.lx, source.line + source.linePos + 1, end - source.linePos - 1);

        t.tp = STRING;
    } else if (validSymbol(first) == 1) {
        // Symbol
        strncpy(t.lx, source.line + source.linePos, 1);

        t.tp = SYMBOL;
    } else {
        // Illegal symbol
        t.tp = ERR;
        t.ec = IllSym;
        strcpy(t.lx, "Error: illegal symbol in source file");
    }

    *back = end;

    return t;
}

int isKeyword(char *lexeme) {
    char keywords[21][20] = {"class", "constructor", "method", "function", "int", "boolean", "char", "void",
                             "var", "static", "field", "let", "do", "if", "else", "while", "return",
                             "true", "false", "null", "this"};

    // Go through every keyword and see if any match
    for (int i = 0; i < 21; i++) {
        if (strcmp(lexeme, keywords[i]) == 0) {
            return 1;
        }
    }

    return 0;
}

int validSymbol(char c) {
    char symbols[19] = {'(', ')', '[', ']', '{', '}', '+', '-', '*', '/', '=', '&', '|', '~', '<', '>', ',', ';', '.'};

    // Go through every symbol and check if c is one of them
    for (int i = 0; i < 19; i++) {
        if (c == symbols[i]) {
            return 1;
        }
    }

    return 0;
}

// IMPLEMENT THE FOLLOWING functions
//***********************************

// Initialise the lexer to read from source file
// file_name is the name of the source file
// This requires opening the file and making any necessary initialisations of the lexer
// If an error occurs, the function should return 0
// if everything goes well the function should return 1
int InitLexer(char* file_name) {
    source.fileName = file_name;
    source.file = fopen(source.fileName, "r");

    if (source.file == NULL) {
        return 0;
    }

    source.lineNumber = 0;
    source.linePos = 1;

    return 1;
}

// Get the next token from the source file
Token GetNextToken() {
    int end;
    Token t = findNextToken(&end);

    // Update position of source.linePos
    if (t.tp != ERR && t.tp != EOFile) {
        t.ec = -1;
        if (t.tp == STRING) {
            t.lx[end-source.linePos-1] = '\0';
            source.linePos = end+1;
        } else {
            t.lx[end-source.linePos] = '\0';
            source.linePos = end;
        }
    }

    return t;
}

// peek (look) at the next token in the source file without removing it from the stream
Token PeekNextToken() {
    int end;
    Token t = findNextToken(&end);

    // Don't update source.linePos
    if (t.tp != ERR && t.tp != EOFile) {
        t.ec = -1;
        if (t.tp == STRING) {
            t.lx[end-source.linePos-1] = '\0';
        } else {
            t.lx[end-source.linePos] = '\0';
        }
    }

    return t;
}

// clean out at end, e.g. close files, free memory, ... etc
int StopLexer() {
    fclose(source.file);

    source.line[0] = '\0';
    source.lineNumber = 0;
    source.linePos = 0;

	return 0;
}

// do not remove the next line
#ifndef TEST
//int main() {
//	// implement your main function here
//    // NOTE: the autograder will not use your main function
//    char tempName[] = "../lexer/Square.jack";
//
//    InitLexer(tempName);
//    Token token = GetNextToken();
//
//    while (token.tp != ERR && token.tp != EOFile) {
//        printf("< %s, %i, %s, %s >\n", token.fl, token.ln, token.lx, types[token.tp]);
//        token = PeekNextToken();
//        printf("< %s, %i, %s, %s >\n", token.fl, token.ln, token.lx, types[token.tp]);
//        token = GetNextToken();
//    }
//
//    printf("< %s, %i, %s, %s >\n", token.fl, token.ln, token.lx, types[token.tp]);
//
//    StopLexer();
//
//	return 0;
//}
// do not remove the next line
#endif
