#ifndef SYMBOLS_H
#define SYMBOLS_H

#include "lexer.h"
#include "parser.h"

// define your own types and function prototypes for the symbol table(s) module below

// Contains data for a single symbol
typedef struct Symbol {
    char name[256];
    char type[256];
    char kind[356];
    int line;
    int number;
} Symbol;

// Contains data for a class
typedef struct Class {
    char name[256];
    int numSymbols;
    Symbol symbols[256];
} Class;

// Contains data for a single method
typedef struct Method {
    char name[256];
    char className[256];
    int numSymbols;
    Symbol symbols[256];
} Method;

// Contains all the data
typedef struct SymbolTable {
    // List of classes and a number to tell how big the array is
    int numClasses;
    Class classes[256];

    // List of methods and a number to tell how big the array is
    int numMethods;
    Method methods[256];
} SymbolTable;

// Stores all methods that have been mentioned and their class
// So that they can be checked that they were declared after parsing
typedef struct Mentioned {
    int numMentioned;
    int lines[256];
    char classNames[256][256];
    char methodNames[256][256];
} Mentioned;

int InitSymbols();
void addJackOS();

int createClass(char *name);
int createMethod(char *name);

int insert(char *name, char *type, char *kind, int line, int location);
int lookUp(char *name);

int checkType(char *type);
void insertMentioned(char *className, char *methodName, int line);
ParserInfo checkMentioned();

ParserInfo checkFinalTypes();

int StopSymbols();

#endif
