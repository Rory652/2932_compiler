
/************************************************************************
University of Leeds
School of Computing
COMP2932- Compiler Design and Construction
The Symbol Tables Module

I confirm that the following code has been developed and written by me and it is entirely the result of my own work.
I also confirm that I have not copied any parts of this program from another person or any other source or facilitated someone to copy this program from me.
I confirm that I will not publish the program online or share it with anyone without permission of the module leader.

Student Name: Rory Coe
Student ID: 201407009
Email: el20rdc@leeds.ac.uk
Date Work Commenced: 02/03/2022
*************************************************************************/

#include "symbols.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

SymbolTable symbolTable;
Mentioned mentioned;

Class *currentClass;
Method *currentMethod;

int InitSymbols() {
    symbolTable.numClasses = 0;
    symbolTable.numMethods = 0;

    mentioned.numMentioned = 0;

    addJackOS();

    return 0;
}

// Adds Jack OS classes to the symbol table
void addJackOS() {
    // Array
    createClass("Array");
    createMethod("new");
    insert("size", "int", "argument", 0, 1);
    createMethod("dispose");

    // Keyboard
    createClass("Keyboard");
    createMethod("keyPressed");
    createMethod("readChar");
    createMethod("readLine");
    insert("message", "String", "argument", 0, 1);
    createMethod("readInt");
    insert("message", "String", "argument", 0, 1);

    // Math
    createClass("Math");
    createMethod("init");
    createMethod("abs");
    insert("x", "int", "argument", 0, 1);
    createMethod("multiply");
    insert("x", "int", "argument", 0, 1);
    insert("y", "int", "argument", 0, 1);
    createMethod("divide");
    insert("x", "int", "argument", 0, 1);
    insert("y", "int", "argument", 0, 1);
    createMethod("min");
    insert("x", "int", "argument", 0, 1);
    insert("y", "int", "argument", 0, 1);
    createMethod("max");
    insert("x", "int", "argument", 0, 1);
    insert("y", "int", "argument", 0, 1);
    createMethod("sqrt");
    insert("x", "int", "argument", 0, 1);

    // Memory
    createClass("Memory");
    createMethod("peek");
    insert("address", "int", "argument", 0, 1);
    createMethod("poke");
    insert("address", "int", "argument", 0, 1);
    insert("value", "int", "argument", 0, 1);
    createMethod("alloc");
    insert("size", "int", "argument", 0, 1);
    createMethod("deAlloc");
    insert("o", "Array", "argument", 0, 1);

    // Output
    createClass("Output");
    createMethod("moveCursor");
    insert("i", "int", "argument", 0, 1);
    insert("j", "int", "argument", 0, 1);
    createMethod("printChar");
    insert("c", "char", "argument", 0, 1);
    createMethod("printString");
    insert("s", "String", "argument", 0, 1);
    createMethod("printInt");
    insert("i", "int", "argument", 0, 1);
    createMethod("println");
    createMethod("backspace");

    // Screen
    createClass("Screen");
    createMethod("clearScreen");
    createMethod("setColor");
    insert("b", "boolean", "argument", 0, 1);
    createMethod("drawPixel");
    insert("x", "int", "argument", 0, 1);
    insert("y", "int", "argument", 0, 1);
    createMethod("drawLine");
    insert("x1", "int", "argument", 0, 1);
    insert("y1", "int", "argument", 0, 1);
    insert("x2", "int", "argument", 0, 1);
    insert("y2", "int", "argument", 0, 1);
    createMethod("drawRectangle");
    insert("x1", "int", "argument", 0, 1);
    insert("y1", "int", "argument", 0, 1);
    insert("x2", "int", "argument", 0, 1);
    insert("y2", "int", "argument", 0, 1);
    createMethod("drawCircle");
    insert("x", "int", "argument", 0, 1);
    insert("y", "int", "argument", 0, 1);
    insert("r", "int", "argument", 0, 1);

    // String
    createClass("String");
    createMethod("new");
    insert("maxLength", "int", "argument", 0, 1);
    createMethod("dispose");
    createMethod("length");
    createMethod("charAt");
    insert("j", "int", "argument", 0, 1);
    createMethod("setCharAt");
    insert("j", "int", "argument", 0, 1);
    insert("c", "char", "argument", 0, 1);
    createMethod("appendChar");
    insert("c", "char", "argument", 0, 1);
    createMethod("eraseLastChar");
    createMethod("intValue");
    createMethod("setInt");
    insert("j", "int", "argument", 0, 1);
    createMethod("backSpace");
    createMethod("doubleQuote");
    createMethod("newLine");

    // Sys
    createClass("Sys");
    createMethod("halt");
    createMethod("error");
    insert("errorCode", "char", "argument", 0, 1);
    createMethod("wait");
    insert("duration", "char", "argument", 0, 1);
}

int createClass(char *name) {
    for (int i = 0; i < symbolTable.numClasses; i++) {
        // Check for redeclaration of classes
        if (strcmp(name, symbolTable.classes[i].name) == 0) {
            return 1;
        }
    }

    symbolTable.numClasses++;

    // Still affects the value in the array - just easier to work with
    currentClass = &symbolTable.classes[symbolTable.numClasses - 1];

    // Copy data to struct
    strcpy(currentClass->name, name);
    currentClass->numSymbols = 0;

    return 0;
}

int createMethod(char *name) {
    for (int i = 0; i < symbolTable.numMethods; i++) {
        // Check for redeclaration of methods
        if (strcmp(name, symbolTable.methods[i].name) == 0 && strcmp(currentClass->name, symbolTable.methods[i].className) == 0) {
            return 1;
        }
    }

    symbolTable.numMethods++;

    // Still affects the value in the array - just easier to work with
    currentMethod = &symbolTable.methods[symbolTable.numMethods - 1];

    // Copy data to struct
    strcpy(currentMethod->name, name);
    strcpy(currentMethod->className, currentClass->name);
    currentMethod->numSymbols = 0;

    // Insert the "this" argument into the method
    insert("this", currentClass->name, "argument", 0, 1);

    return 0;
}

int insert(char *name, char *type, char *kind, int line, int location) {
    int numKind = 0;

    int numSymbols;
    Symbol *symbols = NULL;

    if (location == 0) {
        // Checking in currentClass
        numSymbols = currentClass->numSymbols;
        symbols = currentClass->symbols;
    } else if (location == 1) {
        // Checking in currentMethod
        numSymbols = currentMethod->numSymbols;
        symbols = currentMethod->symbols;
    } else {
        // Error: somehow made a mistaken call to this function
        return -1;
    }

    // Make sure there isn't a redeclaration
    for (int i = 0; i < numSymbols; i++) {
        if (strcmp(name, symbols[i].name) == 0) {
            // Error: redeclaration of variable
            return 1;
        }

        if (strcmp(kind, symbols[i].kind) == 0) {
            numKind++;
        }
    }

    Symbol *temp;

    if (location == 0) {
        // Checking in currentClass
        currentClass->numSymbols++;

        temp = &currentClass->symbols[currentClass->numSymbols - 1];
    } else if (location == 1) {
        // Checking in currentMethod
        currentMethod->numSymbols++;

        temp = &currentMethod->symbols[currentMethod->numSymbols - 1];
    }

    strcpy(temp->name, name);
    strcpy(temp->type, type);
    strcpy(temp->kind, kind);
    temp->line = line;
    temp->number = numKind;

    return 0;
}

int lookUp(char *name) {
    // Check class first
    int numSymbols = currentClass->numSymbols;
    Symbol *symbols = currentClass->symbols;

    for (int i = 0; i < numSymbols; i++) {
        if (strcmp(name, symbols[i].name) == 0) {
            return 0;
        }
    }

    // Check the method
    numSymbols = currentMethod->numSymbols;
    symbols = currentMethod->symbols;

    for (int i = 0; i < numSymbols; i++) {
        if (strcmp(name, symbols[i].name) == 0) {
            return 0;
        }
    }

    // name wasn't found
    return 1;
}

int checkType(char *type) {
    char builtInTypes[3][10] = {"int", "boolean", "char"};

    for (int i = 0; i < 3; i++) {
        if (strcmp(type, builtInTypes[i]) == 0) {
            return 0;
        }
    }

    for (int i = 0; i < symbolTable.numClasses; i++) {
        if (strcmp(type, symbolTable.classes[i].name) == 0) {
            return 0;
        }
    }

    return 1;
}

void insertMentioned(char *className, char *methodName, int line) {
    if (strcmp(className, " ") == 0) {
        className = currentClass->name;
    } else if (checkType(className) == 1) {
        // Assign the correct type to the variable if it isn't one already
        int found = 0;

        for (int i = 0; i < currentClass->numSymbols; i++) {
            if (strcmp(className, currentClass->symbols[i].name) == 0) {
                // Replace the name with the type - not being found isn't necessarily an error
                strcpy(className, currentClass->symbols[i].type);

                found = 1;
                break;
            }
        }

        if (found == 0) {
            for (int i = 0; i < currentMethod->numSymbols; i++) {
                if (strcmp(className, currentMethod->symbols[i].name) == 0) {
                    // Replace the name with the type - not being found isn't necessarily an error
                    strcpy(className, currentMethod->symbols[i].type);

                    found = 1;
                    break;
                }
            }
        }
    }

    for (int i = 0; i < mentioned.numMentioned; i++) {
        if (strcmp(className, mentioned.classNames[i]) == 0 && strcmp(methodName, mentioned.methodNames[i]) == 0) {
            return;
        }
    }

    strcpy(mentioned.classNames[mentioned.numMentioned], className);
    strcpy(mentioned.methodNames[mentioned.numMentioned], methodName);
    mentioned.lines[mentioned.numMentioned] = line;
    mentioned.numMentioned++;
}

ParserInfo checkMentioned() {
    ParserInfo p;
    p.er = none;

    int i, j;
    int found = 0;

    // Check if class/type exists
    for (i = 0; i < mentioned.numMentioned; i++) {
        found = 0;

        for (j = 0; j < symbolTable.numClasses; j++) {
            if (strcmp(mentioned.classNames[i], symbolTable.classes[j].name) == 0) {
                // Found the class
                found = 1;
                break;
            }
        }

        if (found == 0) {
            // Error: class/type not declared
            p.er = undecIdentifier;
            p.tk.ec = NoLexErr;
            p.tk.ln = mentioned.lines[i];
            p.tk.tp = ID;
            strcpy(p.tk.lx, mentioned.classNames[i]);

            return p;
        }
    }

    // Check if method exists within the class
    for (i = 0; i < mentioned.numMentioned; i++) {
        found = 0;

        for (j = 0; j < symbolTable.numMethods; j++) {
            if (strcmp(mentioned.classNames[i], symbolTable.methods[j].className) == 0 && strcmp(mentioned.methodNames[i], symbolTable.methods[j].name) == 0) {
                // Found the method
                found = 1;
                break;
            }
        }

        if (found == 0) {
            // Error: method not declared
            p.er = undecIdentifier;
            p.tk.ec = NoLexErr;
            p.tk.ln = mentioned.lines[i];
            p.tk.tp = ID;
            strcpy(p.tk.lx, mentioned.methodNames[i]);

            return p;
        }
    }

    return p;
}

ParserInfo checkFinalTypes() {
    ParserInfo p;
    p.er = none;

    for (int i = 0; i < symbolTable.numClasses; i++) {
        for (int j = 0; j < symbolTable.classes[i].numSymbols; j++) {
            if (checkType(symbolTable.classes[i].symbols[j].type) == 1) {
                // Error: method not declared
                p.er = undecIdentifier;
                p.tk.ec = NoLexErr;
                p.tk.ln = symbolTable.classes[i].symbols[j].line;
                p.tk.tp = ID;
                strcpy(p.tk.lx, symbolTable.classes[i].symbols[j].type);

                return p;
            }
        }
    }

    for (int i = 0; i < symbolTable.numMethods; i++) {
        for (int j = 0; j < symbolTable.methods[i].numSymbols; j++) {
            if (checkType(symbolTable.methods[i].symbols[j].type) == 1) {
                // Error: method not declared
                p.er = undecIdentifier;
                p.tk.ec = NoLexErr;
                p.tk.ln = symbolTable.methods[i].symbols[j].line;
                p.tk.tp = ID;
                strcpy(p.tk.lx, symbolTable.methods[i].symbols[j].type);

                return p;
            }
        }
    }

    return p;
}

// Clears all the memory used by the symbol table
int StopSymbols() {
    return 0;
}


