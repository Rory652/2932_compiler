#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "symbols.h"

#include "parser.h"

// you can declare prototypes of parser functions below

char syntaxErrors[16][50] = {"none", "lexerErr", "classExpected", "idExpected", "openBraceExpected", "closeBraceExpected",
                         "memberDeclarErr", "classVarErr", "illegalType", "semicolonExpected", "subroutineDeclarErr",
                         "openParenExpected", "closeParenExpected", "closeBracketExpected",
                         "equalExpected", "syntaxError"};

// Prints a token out
void printToken(Token tk) {
    printf("<%i, %s, %s>\n", tk.ln, tk.fl, tk.lx);
}

// Function Declarations
ParserInfo returnError(ParserInfo pi, SyntaxErrors se);

ParserInfo classDeclar();
ParserInfo memberDeclar();
ParserInfo classVarDeclar();
ParserInfo type();
ParserInfo subroutineDeclar();
ParserInfo paramList();
ParserInfo subroutineBody();
ParserInfo statement();
ParserInfo varDeclarStatement();
ParserInfo letStatment();
ParserInfo ifStatement();
ParserInfo whileStatement();
ParserInfo doStatement();
ParserInfo subroutineCall();
ParserInfo expressionList();
ParserInfo returnStatment();
ParserInfo expression();
ParserInfo relationalExpression();
ParserInfo arithmeticExpression();
ParserInfo term();
ParserInfo factor();
ParserInfo operand();

ParserInfo returnError(ParserInfo pi, SyntaxErrors se) {
    if (pi.tk.tp == ERR) {
        pi.er = lexerErr;
    } else {
        pi.er = se;
    }

    return pi;
}

/*
 * A | B : choice between A or B
 * {A} : zero or more occurrences of A
 * [A] : zero or one occurrence of A
 * ( ) : parentheses, used to raise the precedence of the | metasymbol
*/

// classDeclar  'class' 'identifier' '{' {memberDeclar} '}'
ParserInfo classDeclar() {
    ParserInfo pi;
    pi.er = none;

    Token tk = GetNextToken();

    if (strcmp(tk.lx, "class") == 0) {
        tk = GetNextToken();
        if (tk.tp == ID) {
            if (createClass(tk.lx) == 1) {
                pi.tk = tk;
                returnError(pi, redecIdentifier);
            }

            tk = GetNextToken();
            if (tk.lx[0] == '{') {
                tk = PeekNextToken();

                while (strcmp(tk.lx, "static") == 0 || strcmp(tk.lx, "field") == 0 ||
                    strcmp(tk.lx, "constructor") == 0 || strcmp(tk.lx, "function") == 0 || strcmp(tk.lx, "method") == 0) {
                    pi = memberDeclar();
                    if (pi.er != none) {
                        return pi;
                    }
                    tk = PeekNextToken();
                }

                if (tk.tp == ERR) {
                    pi.er = lexerErr;
                    pi.tk = tk;
                    return pi;
                }

                tk = GetNextToken();
                if (tk.lx[0] == '}') {
                    pi.tk = tk;
                    return pi;
                } else {
                    pi.tk = tk;
                    return returnError(pi, closeBraceExpected);
                }
            } else {
                pi.tk = tk;
                return returnError(pi, openBraceExpected);
            }
        } else {
            pi.tk = tk;
            return returnError(pi, idExpected);
        }
    } else {
        pi.tk = tk;
        return returnError(pi, classExpected);
    }
}

// memberDeclar  classVarDeclar | subroutineDeclar
ParserInfo memberDeclar() {
    ParserInfo pi;
    pi.er = none;

    Token tk = PeekNextToken();

    if (strcmp(tk.lx, "static") == 0 || strcmp(tk.lx, "field") == 0) {
        return classVarDeclar();
    } else if (strcmp(tk.lx, "constructor") == 0 || strcmp(tk.lx, "function") == 0 || strcmp(tk.lx, "method") == 0) {
        return subroutineDeclar();
    } else {
        pi.tk = tk;
        return returnError(pi,memberDeclarErr);
    }
}

// classVarDeclar  ('static' | 'field') type 'identifier' {',' 'identifier'} ';'
ParserInfo classVarDeclar() {
    ParserInfo pi;
    pi.er = none;

    Token tk = GetNextToken();

    if (strcmp(tk.lx, "static") == 0 || strcmp(tk.lx, "field") == 0) {
        char kind[256];
        strcpy(kind, tk.lx);

        pi = type();
        char *argType = pi.tk.lx;

        if (pi.er == none) {
            tk = GetNextToken();
            if (tk.tp == ID) {
                if (insert(tk.lx, argType, kind, tk.ln, 0) == 1) {
                    pi.tk = tk;
                    return returnError(pi, redecIdentifier);
                }

                tk = PeekNextToken();

                while (tk.lx[0] == ',') {
                    GetNextToken();

                    tk = GetNextToken();
                    if (tk.tp != ID) {
                        pi.tk = tk;
                        return returnError(pi, idExpected);
                    }

                    if (insert(tk.lx, argType, kind, tk.ln, 0) == 1) {
                        pi.tk = tk;
                        return returnError(pi, redecIdentifier);
                    }

                    tk = PeekNextToken();
                }

                if (tk.tp == ERR) {
                    pi.er = lexerErr;
                    pi.tk = tk;
                    return pi;
                }

                tk = GetNextToken();
                if (tk.lx[0] == ';') {
                    pi.tk = tk;
                    return pi;
                } else {
                    pi.tk = tk;
                    return returnError(pi, semicolonExpected);
                }
            } else {
                pi.tk = tk;
                return returnError(pi, idExpected);
            }
        } else {
            return pi;
        }
    } else {
        pi.tk = tk;
        return returnError(pi, classVarErr);
    }
}

// type  'int' | 'char' | 'boolean' | 'identifier'
ParserInfo type() {
    ParserInfo pi;
    pi.er = none;

    pi.tk = PeekNextToken();
    if ((strcmp(pi.tk.lx, "int") == 0 || strcmp(pi.tk.lx, "char") == 0 ||
        strcmp(pi.tk.lx, "boolean") == 0 || pi.tk.tp == ID)) {
        // Burn the token - useful paramList
        pi.tk = GetNextToken();
    } else {
        return returnError(pi, illegalType);
    }

    return pi;
}

// subroutineDeclar  ('constructor' | 'function' | 'method') (type|'void') 'identifier' '(' paramList ')' subroutineBody
ParserInfo subroutineDeclar() {
    ParserInfo pi;
    pi.er = none;

    Token tk = GetNextToken();

    if (strcmp(tk.lx, "constructor") == 0 || strcmp(tk.lx, "function") == 0 || strcmp(tk.lx, "method") == 0) {
        tk = PeekNextToken();
        if (strcmp(tk.lx, "void") == 0) {
            // Burn the token
            GetNextToken();
        } else {
            pi = type();
            if (pi.er != none) {
                return pi;
            }
        }

        tk = GetNextToken();
        if (tk.tp == ID) {
            if (createMethod(tk.lx) == 1) {
                pi.tk = tk;
                returnError(pi, redecIdentifier);
            }

            tk = GetNextToken();
            if (tk.lx[0] == '(') {
                pi = paramList();
                if (pi.er != none) {
                    return pi;
                }

                tk = GetNextToken();
                if (tk.lx[0] == ')') {
                    return subroutineBody();
                } else {
                    pi.tk = tk;
                    return returnError(pi, closeParenExpected);
                }
            } else {
                pi.tk = tk;
                return returnError(pi, openParenExpected);
            }
        } else {
            pi.tk = tk;
            return returnError(pi, idExpected);
        }
    } else {
        pi.tk = tk;
        return returnError(pi, subroutineDeclarErr);
    }

}

// paramList  type 'identifier' {',' type 'identifier'} | ε
ParserInfo paramList() {
    ParserInfo pi;

    Token tk = PeekNextToken();

    pi = type();
    char *argType = pi.tk.lx;

    if (pi.er == none) {
        tk = GetNextToken();
        if (tk.tp == ID) {
            if (insert(tk.lx, argType, "argument", tk.ln, 1) == 1) {
                pi.tk = tk;
                return returnError(pi, redecIdentifier);
            }

            tk = PeekNextToken();

            while (tk.lx[0] == ',') {
                GetNextToken();

                pi = type();
                if (pi.er != none) {
                    return pi;
                }

                tk = GetNextToken();
                if (tk.tp != ID) {
                    pi.tk = tk;
                    return returnError(pi, idExpected);
                }

                argType = pi.tk.lx;
                if (insert(tk.lx, argType, "argument", tk.ln, 1) == 1) {
                    pi.tk = tk;
                    return returnError(pi, redecIdentifier);
                }

                tk = PeekNextToken();
            }

            if (tk.tp == ERR) {
                pi.er = lexerErr;
                pi.tk = tk;
                return pi;
            }

            return pi;
        } else {
            pi.tk = tk;
            return returnError(pi, idExpected);
        }
    } else {
        pi.er = none;
        pi.tk = tk;
        return pi;
    }
}

// subroutineBody  '{' {statement} '}'
ParserInfo subroutineBody() {
    ParserInfo pi;
    pi.er = none;

    Token tk = GetNextToken();

    if (tk.lx[0] == '{') {
        tk = PeekNextToken();

        while (strcmp(tk.lx, "var") == 0 || strcmp(tk.lx, "let") == 0 || strcmp(tk.lx, "if") == 0 ||
                strcmp(tk.lx, "while") == 0 || strcmp(tk.lx, "do") == 0 || strcmp(tk.lx, "return") == 0) {
            pi = statement();
            if (pi.er != none) {
                return pi;
            }
            tk = PeekNextToken();
        }

        if (tk.tp == ERR) {
            pi.er = lexerErr;
            pi.tk = tk;
            return pi;
        }

        tk = GetNextToken();
        if (tk.lx[0] == '}') {
            pi.tk = tk;
            return pi;
        } else {
            pi.tk = tk;
            return returnError(pi, closeBraceExpected);
        }
    } else {
        pi.tk = tk;
        return returnError(pi, openBraceExpected);
    }
}

// statement  varDeclarStatement | letStatment | ifStatement | whileStatement | doStatement | returnStatment
ParserInfo statement() {
    ParserInfo pi;
    pi.er = none;

    Token tk = PeekNextToken();

    if (strcmp(tk.lx, "var") == 0) {
        return varDeclarStatement();
    } else if (strcmp(tk.lx, "let") == 0) {
        return letStatment();
    } else if (strcmp(tk.lx, "if") == 0) {
        return ifStatement();
    } else if (strcmp(tk.lx, "while") == 0) {
        return whileStatement();
    } else if (strcmp(tk.lx, "do") == 0) {
        return doStatement();
    } else if (strcmp(tk.lx, "return") == 0) {
        return returnStatment();
    } else {
        pi.tk = tk;
        return returnError(pi, syntaxError);
    }
}

// varDeclarStatement  'var' type 'identifier' { ',' 'identifier' } ';'
ParserInfo varDeclarStatement() {
    ParserInfo pi;
    pi.er = none;

    Token tk = GetNextToken();

    if (strcmp(tk.lx, "var") == 0) {
        pi = type();

        char argType[256];
        strcpy(argType, pi.tk.lx);

        if (pi.er == none) {
            tk = GetNextToken();

            if (tk.tp == ID) {
                if (insert(tk.lx, argType, "var", tk.ln, 1) == 1) {
                    pi.tk = tk;
                    return returnError(pi, redecIdentifier);
                }

                tk = PeekNextToken();

                while (tk.lx[0] == ',') {
                    GetNextToken();

                    tk = GetNextToken();
                    if (tk.tp != ID) {
                        pi.tk = tk;
                        return returnError(pi, idExpected);
                    }

                    if (insert(tk.lx, argType, "var", tk.ln, 1) == 1) {
                        pi.tk = tk;
                        return returnError(pi, redecIdentifier);
                    }
                    tk = PeekNextToken();
                }

                if (tk.tp == ERR) {
                    pi.er = lexerErr;
                    pi.tk = tk;
                    return pi;
                }

                tk = GetNextToken();
                if (tk.lx[0] == ';') {
                    pi.tk = tk;
                    return pi;
                } else {
                    pi.tk = tk;
                    return returnError(pi, semicolonExpected);
                }
            } else {
                pi.tk = tk;
                return returnError(pi, idExpected);
            }
        } else {
            return pi;
        }
    } else {
        pi.tk = tk;
        return returnError(pi, syntaxError);
    }
}

// letStatment  'let' 'identifier' [ '[' expression ']' ] '=' expression ';'
ParserInfo letStatment() {
    ParserInfo pi;
    pi.er = none;

    Token tk = GetNextToken();

    if (strcmp(tk.lx, "let") == 0) {
        tk = GetNextToken();

        if (tk.tp == ID) {
            if (lookUp(tk.lx) == 1) {
                pi.tk = tk;
                return returnError(pi, undecIdentifier);
            }

            tk = PeekNextToken();

            if (tk.lx[0] == '[') {
                GetNextToken();

                pi = expression();
                if (pi.er != none) {
                    return pi;
                }

                tk = GetNextToken();

                if (tk.lx[0] != ']') {
                    pi.tk = tk;
                    return returnError(pi, closeBracketExpected);
                }
            }

            tk = GetNextToken();

            if (tk.lx[0] == '=') {
                pi = expression();
                if (pi.er != none) {
                    return pi;
                }

                tk = GetNextToken();
                if (tk.lx[0] == ';') {
                    pi.tk = tk;
                    return pi;
                } else {
                    pi.tk = tk;
                    return returnError(pi, semicolonExpected);
                }
            } else {
                pi.tk = tk;
                return returnError(pi, equalExpected);
            }
        } else {
            pi.tk = tk;
            return returnError(pi, idExpected);
        }
    } else {
        pi.tk = tk;
        return returnError(pi, syntaxError);
    }
}

// ifStatement  'if' '(' expression ')' '{' {statement} '}' ['else' '{' {statement} '}']
ParserInfo ifStatement() {
    ParserInfo pi;
    pi.er = none;

    Token tk = GetNextToken();

    if (strcmp(tk.lx, "if") == 0) {
        tk = GetNextToken();

        if (tk.lx[0] == '(') {
            pi = expression();
            if (pi.er != none) {
                return pi;
            }

            tk = GetNextToken();

            if (tk.lx[0] == ')') {
                tk = GetNextToken();
                if (tk.lx[0] == '{') {
                    tk = PeekNextToken();

                    while (tk.lx[0] != '}') {
                        pi = statement();
                        if (pi.er != none) {
                            return pi;
                        }
                        tk = PeekNextToken();
                    }

                    if (tk.tp == ERR) {
                        pi.er = lexerErr;
                        pi.tk = tk;
                        return pi;
                    }

                    tk = GetNextToken();
                    if (tk.lx[0] == '}') {
                        tk = PeekNextToken();

                        if (strcmp(tk.lx, "else") == 0) {
                            GetNextToken();

                            tk = PeekNextToken();
                            if (tk.lx[0] == '{') {
                                GetNextToken();

                                tk = PeekNextToken();

                                while (tk.lx[0] != '}') {
                                    pi = statement();
                                    if (pi.er != none) {
                                        return pi;
                                    }
                                    tk = PeekNextToken();
                                }

                                if (tk.tp == ERR) {
                                    pi.er = lexerErr;
                                    pi.tk = tk;
                                    return pi;
                                }

                                tk = GetNextToken();
                                if (tk.lx[0] != '}') {
                                    pi.tk = tk;
                                    return returnError(pi, closeBraceExpected);
                                }
                            } else {
                                pi.tk = tk;
                                return returnError(pi, openBraceExpected);
                            }
                        }

                        pi.tk = tk;
                        return pi;
                    } else {
                        pi.tk = tk;
                        return returnError(pi, closeBraceExpected);
                    }
                } else {
                    pi.tk = tk;
                    return returnError(pi, openBraceExpected);
                }
            } else {
                pi.tk = tk;
                return returnError(pi, closeParenExpected);
            }
        } else {
            pi.tk = tk;
            return returnError(pi, openParenExpected);
        }
    } else {
        pi.tk = tk;
        return returnError(pi, syntaxError);
    }
}

// whileStatement  'while' '(' expression ')' '{' {statement} '}'
ParserInfo whileStatement() {
    ParserInfo pi;
    pi.er = none;

    Token tk = GetNextToken();

    if (strcmp(tk.lx, "while") == 0) {
        tk = GetNextToken();

        if (tk.lx[0] == '(') {
            pi = expression();
            if (pi.er != none) {
                return pi;
            }

            tk = GetNextToken();

            if (tk.lx[0] == ')') {
                tk = GetNextToken();
                if (tk.lx[0] == '{') {
                    tk = PeekNextToken();

                    while (tk.lx[0] != '}') {
                        pi = statement();
                        if (pi.er != none) {
                            return pi;
                        }
                        tk = PeekNextToken();
                    }

                    if (tk.tp == ERR) {
                        pi.er = lexerErr;
                        pi.tk = tk;
                        return pi;
                    }

                    tk = GetNextToken();
                    if (tk.lx[0] == '}') {
                        pi.tk = tk;
                        return pi;
                    } else {
                        pi.tk = tk;
                        return returnError(pi, closeBraceExpected);
                    }
                } else {
                    pi.tk = tk;
                    return returnError(pi, openBraceExpected);
                }
            } else {
                pi.tk = tk;
                return returnError(pi, closeParenExpected);
            }
        } else {
            pi.tk = tk;
            return returnError(pi, openParenExpected);
        }
    } else {
        pi.tk = tk;
        return returnError(pi, syntaxError);
    }
}

// doStatement  'do' subroutineCall ;
ParserInfo doStatement() {
    ParserInfo pi;
    pi.er = none;

    Token tk = GetNextToken();

    if (strcmp(tk.lx, "do") == 0) {
        pi = subroutineCall();
        if (pi.er == none) {
            tk = GetNextToken();
            if (tk.lx[0] == ';') {
                pi.tk = tk;
                return pi;
            } else {
                pi.tk = tk;
                return returnError(pi, semicolonExpected);
            }
        } else {
            return pi;
        }
    } else {
        pi.tk = tk;
        return returnError(pi, syntaxError);
    }
}

// subroutineCall  'identifier' [ '.' 'identifier' ] '(' expressionList ')'
ParserInfo subroutineCall() {
    ParserInfo pi;
    pi.er = none;

    Token tk = GetNextToken();
    // Used for the symbol table
    Token temp = tk;

    if (tk.tp == ID) {
        tk = PeekNextToken();
        if (tk.lx[0] == '.') {
            // Burn the token
            GetNextToken();

            tk = GetNextToken();
            if (tk.tp != ID) {
                pi.tk = tk;
                return returnError(pi, idExpected);
            } else {
                insertMentioned(temp.lx, tk.lx, tk.ln);
            }
        } else {
            insertMentioned(" ", temp.lx, temp.ln);
        }

        tk = GetNextToken();
        if (tk.lx[0] == '(') {
            pi = expressionList();
            if (pi.er != none) {
                return pi;
            }

            tk = GetNextToken();
            if (tk.lx[0] == ')') {
                pi.tk = tk;
                return pi;
            } else {
                pi.tk = tk;
                return returnError(pi, closeParenExpected);
            }
        } else {
            pi.tk = tk;
            return returnError(pi, openParenExpected);
        }
    } else {
        pi.tk = tk;
        return returnError(pi, idExpected);
    }
}

// expressionList  expression { ',' expression } | 'ε'
ParserInfo expressionList() {
    ParserInfo pi;
    pi = expression();

    Token tk;

    if (pi.er == none) {
        tk = PeekNextToken();

        while (tk.lx[0] == ',') {
            GetNextToken();

            pi = expression();
            if (pi.er != none) {
                return pi;
            }
            tk = PeekNextToken();
        }

        return pi;
    } else {
        pi.er = none;
        return pi;
    }
}

// returnStatment  'return' [ expression ] ';'
ParserInfo returnStatment() {
    ParserInfo pi;
    pi.er = none;

    Token tk = GetNextToken();

    if (strcmp(tk.lx, "return") == 0) {
        pi = expression();

        tk = GetNextToken();
        if (tk.lx[0] == ';') {
            // Set in case expression() returns an error
            pi.er = none;
            pi.tk = tk;
            return pi;
        } else {
            pi.tk = tk;
            return returnError(pi, semicolonExpected);
        }
    } else {
        pi.tk = tk;
        return returnError(pi, syntaxError);
    }
}

// expression  relationalExpression { ( '&' | '|' ) relationalExpression }
ParserInfo expression() {
    ParserInfo pi;
    pi.er = none;

    pi = relationalExpression();
    if (pi.er == none) {
        Token tk = PeekNextToken();
        while (tk.lx[0] == '&' || tk.lx[0] == '|') {
            GetNextToken();

            pi = relationalExpression();
            if (pi.er != none) {
                return pi;
            }

            tk = PeekNextToken();
        }
    }

    return pi;
}

// relationalExpression  arithmeticExpression { ( '=' | '>' | '<' ) arithmeticExpression }
ParserInfo relationalExpression() {
    ParserInfo pi;
    pi.er = none;
    pi = arithmeticExpression();

    if (pi.er == none) {
        Token tk = PeekNextToken();
        while (tk.lx[0] == '=' || tk.lx[0] == '>' || tk.lx[0] == '<') {
            GetNextToken();

            pi = arithmeticExpression();
            if (pi.er != none) {
                return pi;
            }

            tk = PeekNextToken();
        }
    }

    return pi;
}

// arithmeticExpression  term { ( '+' | '-' ) term }
ParserInfo arithmeticExpression() {
    ParserInfo pi;
    pi.er = none;
    pi = term();

    if (pi.er == none) {
        Token tk = PeekNextToken();
        while (tk.lx[0] == '+' || tk.lx[0] == '-') {
            GetNextToken();

            pi = term();
            if (pi.er != none) {
                return pi;
            }

            tk = PeekNextToken();
        }
    }

    return pi;
}

// term  factor { ( '*' | '/' ) factor }
ParserInfo term() {
    ParserInfo pi;
    pi.er = none;

    pi = factor();
    if (pi.er == none) {
        Token tk = PeekNextToken();
        while (tk.lx[0] == '*' || tk.lx[0] == '/') {
            GetNextToken();

            pi = factor();
            if (pi.er != none) {
                return pi;
            }

            tk = PeekNextToken();
        }
    }

    return pi;
}

// factor  ( '-' | '~' | 'ε' ) operand
ParserInfo factor() {
    Token tk = PeekNextToken();
    if (tk.tp == SYMBOL && (tk.lx[0] == '-' || tk.lx[0] == '~')) {
        GetNextToken();
    }

    return operand();
}

// operand  'integerConstant' | 'identifier' ['.' 'identifier'] [ '[' expression ']' | '(' expressionList ')' ] | '(' expression ')' | 'stringLiteral' | 'true' | 'false' | 'null' | 'this'
ParserInfo operand() {
    ParserInfo pi;
    pi.er = none;

    Token tk = PeekNextToken();
    if (tk.tp == INT) {
        GetNextToken();
        pi.tk = tk;
        return pi;
    } else if (tk.tp == ID) {
        Token temp = GetNextToken();

        tk = PeekNextToken();
        if (tk.lx[0] == '.') {
            GetNextToken();

            tk = GetNextToken();
            if (tk.tp != ID) {
                pi.tk = tk;
                return returnError(pi, idExpected);
            } else if (PeekNextToken().lx[0] == '(') {
                // If it's a function, insert it into mentioned
                insertMentioned(temp.lx, tk.lx, tk.ln);
            }
        } else {
            // Check identifier exists
            if (lookUp(temp.lx) == 1) {
                pi.tk = temp;
                return returnError(pi, undecIdentifier);
            }
        }

        tk = PeekNextToken();
        if (tk.lx[0] == '[') {
            GetNextToken();

            pi = expression();
            if (pi.er != none) {
                return pi;
            }

            tk = GetNextToken();
            if (tk.lx[0] != ']') {
                pi.tk = tk;
                return returnError(pi, closeBracketExpected);
            }
        } else if (tk.lx[0] == '(') {
            GetNextToken();

            pi = expressionList();
            if (pi.er != none) {
                return pi;
            }

            tk = GetNextToken();
            if (tk.lx[0] != ')') {
                pi.tk = tk;
                return returnError(pi, closeParenExpected);
            }
        }

        return pi;
    } else if (tk.lx[0] == '(') {
        GetNextToken();

        pi = expression();
        if (pi.er == none) {
            tk = GetNextToken();
            if (tk.lx[0] == ')') {
                pi.tk = tk;
                return pi;
            } else {
                pi.tk = tk;
                return returnError(pi, closeParenExpected);
            }
        } else {
            return pi;
        }
    } else if (tk.tp == STRING) {
        GetNextToken();
        pi.tk = tk;
        return pi;
    } else if (strcmp(tk.lx, "true") == 0) {
        GetNextToken();
        pi.tk = tk;
        return pi;
    } else if (strcmp(tk.lx, "false") == 0) {
        GetNextToken();
        pi.tk = tk;
        return pi;
    } else if (strcmp(tk.lx, "null") == 0) {
        GetNextToken();
        pi.tk = tk;
        return pi;
    } else if (strcmp(tk.lx, "this") == 0) {
        GetNextToken();
        pi.tk = tk;
        return pi;
    } else {
        pi.tk = tk;
        return returnError(pi, syntaxError);
    }
}

int InitParser(char* file_name) {
    return InitLexer(file_name);
}

ParserInfo Parse() {
    ParserInfo pi;

    pi = classDeclar();

    return pi;
}


int StopParser() {
    return StopLexer();
}

//#ifndef TEST_PARSER
//int main() {
//    char tempName[] = "../Jack Files/idExpected2.jack";
//
//    InitParser(tempName);
//
//    Parse();
//
//    StopParser();
//
//    return 0;
//}
//#endif
